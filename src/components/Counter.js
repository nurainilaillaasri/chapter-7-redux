// import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import classes from './Counter.module.css';

const Counter = () => {
  // const [counter, setCounter] = useState(0);
  const dispacth = useDispatch();
  const counter = useSelector(state => state.counter);
  const show = useSelector(state => state.showCounter);

  const incrementNumber = () => {
    dispacth({ type: 'increment' });
  }

  const increaseByUserInput = () => {
    dispacth({ type: 'increaseByUserInput', value: 10 });
  }

  const decrementNumber = () => {
    dispacth({ type: 'decrement' });
  }

  const toggleCounterHandler = () => {
    dispacth({ type: 'toogleCounter' });
  };

  return (
    <main className={classes.counter}>
      <h1>Redux Counter</h1>
      {show && <div className={classes.value}>{counter}</div>}
      <div>
        <button onClick={incrementNumber}>Increment</button>
        <button onClick={increaseByUserInput}>Increase by 10</button>
        <button onClick={decrementNumber}>Decrement</button>
      </div>
      <button onClick={toggleCounterHandler}>Toggle Counter</button>
    </main>
  );
};

export default Counter;
