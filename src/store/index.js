import { createStore } from 'redux';
import { composeWithDevTools } from "redux-devtools-extension";

const initialState = { counter: 0, showCounter: true };

const counterReducer = (state = initialState, action) => {
  if(action.type === 'increment') {
    return {
      counter: state.counter + 1,
      showCounter: state.showCounter
    }
  }
  
  if(action.type === 'increaseByUserInput') {
    return {
      counter: state.counter + action.value,
      showCounter: state.showCounter
    }
  }

  if(action.type === 'decrement') {
    return {
      counter: state.counter - 1,
      showCounter: state.showCounter
    }
  }

  if(action.type === 'toogleCounter') {
    return {
      showCounter: !state.showCounter,
      counter: state.counter,
    }
  }

  return state;
}

const store = createStore(counterReducer, composeWithDevTools());


export default store;